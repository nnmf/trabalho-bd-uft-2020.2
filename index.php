<?php
include('templates/header.php');
?>

<div class="container" style="padding-top: 30px;">
    <div class="jumbotron">
        <div class="row">
            <div class="col-sm-4">
                <img src="assets/images/logo-main.jpeg" height="300px">
            </div>
            <div class="col-sm-8">
                <h1 class="display-4">Catálago de livros</h1>
                <p class="lead">Trabalho de Banco de Dados com o intuito de criar um catálogo CRUD simples de Livros e Autores feito por: Neudison Nonato Maia Filho e Kayo Aurélio Severino Nunes.</p>
            </div>
        </div>
    </div>
</div>

<?php
include('templates/footer.php');
?>