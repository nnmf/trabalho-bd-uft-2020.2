<?php

use ConexaoPHPPostgres\AuthorAndBooksModel;
use ConexaoPHPPostgres\AuthorModel;
use ConexaoPHPPostgres\BookModel;

include '../../database/models.php';
include_once '../../database/database.ini.php';


$bookModel = new BookModel($pdo);
$authorAndBooks = new AuthorAndBooksModel($pdo);

$id = 0;

if (!empty($_GET['id'])) {
    $id = $_REQUEST['id'];
    $book = $bookModel->get_by_id($id);
}


if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $id = $_POST['id'];
    try {
        $authorAndBooks->delete_by_book_id($id);
        $bookModel->delete_by_id($id);
        header("Location: ../../pages/books.php");
    } catch (PDOException $e) {
        $error = $e->getMessage();
    }
}

?>
<?php
include('../../templates/header.php');
?>

<?php if (!empty($book)) : ?>
    <div class="container">

        <div class="row py-5">
            <div class="col"><a href="../books.php"><img src="../../assets/images/backbutton.png" height="30px"></a></div>
            <div class="col">
                <h4>Excluir livro</h4>
            </div>
            <div class="col"></div>
        </div>

        <div class="span10">
            <div style="padding-top: 10px;">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Nome:  <?php echo htmlspecialchars($book['name']); ?></h5>
                        <form class="form-horizontal" action="book.php?id=<?php echo $id; ?>" method="post">
                            <!-- Alerta em caso de erro -->
                            <?php if (!empty($error)) : ?>
                                <span class="text-danger"><?php echo $error; ?></span>
                            <?php endif; ?>
                            <input type="hidden" name="id" value="<?php echo $id; ?>" />
                            <div class="alert  alert-danger" role="alert">
                                <h5> Deseja excluir esse autor? </h5>
                                <div class="form actions">
                                    <button type="submit" class="btn btn-danger"> Sim </button>
                                    <a href="../books.php" type="btn" class="btn btn-default"> Não </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php
include('../../templates/footer.php');
?>