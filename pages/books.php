<?php
include '../database/models.php';
include_once '../database/database.ini.php';

use ConexaoPHPPostgres\GenderModel as GenderModel;
use ConexaoPHPPostgres\BookModel as BookModel;

try {
    $bookModel = new BookModel($pdo);
    $genderModel = new GenderModel($pdo);

    $booksList = $bookModel->all();
} catch (\PDOException $e) {
    echo $e->getMessage();
}
?>
<?php
include('../templates/header.php');
?>

<br>
<div class="container">
    <div class="row">
        <div class="col-auto mr-auto">
            <h1 style="padding-top: 10px; padding-bottom:10px">Livros</h1>
        </div>
        <div class="col-auto">
            <div class="text-right mb-4">
                <a class="btn" style="background-color: #00897c; color:white" href="../../pages/create/book.php">Cadastrar novo</a>
            </div>
        </div>
    </div>
<?php if (!empty($booksList)) : ?>
    <?php foreach ($booksList as $book) : ?>
        <div>
            <div class="card-body" style="background-color: #F4F6FC;">
                <h4 class="alert-heading"><?php echo htmlspecialchars($book['name']); ?></h4>
                <p> <img src="../assets/icons/profile-fill.png">
                    Genero: <?php
                                    $gender = $genderModel->select_by_id($book['gender_id']);
                                    echo htmlspecialchars($gender['name']);
                                    ?>
                </p>
                <hr>
                <div>
                    <a href="../../pages/update/book.php?id=<?php echo $book['id']; ?>" class="card-link">Editar</a>
                    <a href="../../pages/delete/book.php?id=<?php echo $book['id']; ?>" class="card-link">Remover</a
                </div>

            </div>
        </div>
    <?php endforeach; ?>
</div>

<?php else: ?>

    <div class="row">
        <div class="col" style="text-align: center;font-size: xx-large;padding-top: 40px">
            <h1>Lista Vazia</h1>
        </div>
        <div class="col col-lg-1">

        </div>
    </div>

<?php endif; ?>

<?php
include('../templates/footer.php');
?>