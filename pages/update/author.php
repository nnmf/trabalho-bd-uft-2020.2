<?php

use ConexaoPHPPostgres\AuthorModel;

include '../../database/models.php';
include_once '../../database/database.ini.php';

$authorModel = new AuthorModel($pdo);


$name = null;
$birth = null;
$sex = null;
$id = null;


if (!empty($_GET['id'])) {
    $id = $_REQUEST['id'];
    $author = $authorModel->select_by_id($id);

    $name = $author['name'];
    $birth = $author['birth'];
    $sex = $author['sex'];
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $name = $_REQUEST['name'];
    $birth = $_REQUEST['birth'];
    $sex = $_REQUEST['sex'];
    $id = $_REQUEST['id'];

    try {
        $authorModel->update($name,$birth,$sex,$id);
        header("Location: ../../pages/authors.php");
    } catch (PDOException $e) {
        printf($e->getMessage());
        $error = $e->getMessage();
    }
}

?>
<?php
include('../../templates/header.php');
?>

    <div class="container">

        <div class="row py-5">
            <div class="col"><a href="../authors.php"><img src="../../assets/images/backbutton.png" height="40px"></a></div>
            <div class="col">
                <h4>Cadastrar novo autor</h4>
            </div>
            <div class="col"></div>
        </div>

        <form action="author.php" method="post">

            <input type="hidden" id="id" name="id" value="<?php echo !empty($id) ? $id : ''; ?>">

            <!-- Alerta em caso de erro -->
            <?php if (!empty($error)) : ?>
                <span class="text-danger"><?php echo $error; ?></span>
            <?php endif; ?>

            <div class="form-group">
                <label for="name">Nome completo:</label>
                <input class="form-control" value="<?php echo !empty($name) ? $name : ''; ?>" type="text" name="name" id="name" required>
            </div>

            <div class="form-group">
                <label for="Sex">Sexo:</label>
                <br>
                <input type="radio" id="male" name="sex" value="M" <?php echo $sex === 'M' ? "checked" : '' ?> required>
                <label for="male">Masculino</label><br>
                <input type="radio" id="female" name="sex" value="F" <?php echo $sex === 'F' ? "checked" : '' ?>>
                <label for="female">Feminino</label><br>
                <input type="radio" id="other" name="sex" value="O" <?php echo $sex == 'O' ? "checked" : '' ?>>
                <label for="other">Outro</label>
            </div>


            <div class="form-group">
                <label for="birth">Data Nascimento:</label>
                <input class="form-control" type="date" value="<?php echo !empty($birth) ? $birth : ''; ?>" name="birth" id="birth" required>
            </div>

            <input class="btn btn-primary" type="submit" value="Cadastrar">

        </form>
    </div>
<?php
include('../../templates/footer.php');
?>