<?php
include '../../database/models.php';
include_once '../../database/database.ini.php';

use ConexaoPHPPostgres\AuthorAndBooksModel;
use ConexaoPHPPostgres\AuthorModel;
use ConexaoPHPPostgres\BookModel;
use ConexaoPHPPostgres\GenderModel;

$genderModel = new GenderModel($pdo);
$authorModel = new AuthorModel($pdo);
$authorAndBooksModel = new AuthorAndBooksModel($pdo);

$gendersList = $genderModel->all();
$authorsList = $authorModel->all();

$name = null;
$genderId = null;
$authorId = null;

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $name = $_REQUEST['name'];
    $genderId = $_REQUEST['gender'];
    $authorId = $_REQUEST['author'];



    try {
        $bookModel = new BookModel($pdo);
        $bookModel->insert($name, $genderId);
        $bookId = $bookModel->get_id_by_name($name);
        $authorAndBooksModel->insert($authorId,$bookId['id']);
        header("Location: ../../pages/books.php");
    } catch (PDOException $e) {
        $error = $e->getMessage();
    }
}

?>
<?php
include('../../templates/header.php');
?>

<div class="container">

    <div class="row py-5">
        <div class="col"><a href="../books.php"><img src="../../assets/images/backbutton.png" height="40px"></a></div>
        <div class="col">
            <h4>Adicionar novo Livro</h4>
        </div>
        <div class="col"></div>
    </div>

    <form action="book.php" method="post">

        <!-- Alerta em caso de erro -->
        <?php if (!empty($error)) : ?>
            <span class="text-danger"><?php echo $error; ?></span>
        <?php endif; ?>

        <div class="form-group">
            <label for="name">Nome do livro:</label>
            <input class="form-control" value="<?php echo !empty($name) ? $name : ''; ?>" type="text" name="name" id="name" required>
        </div>

        <div class="form-group">
            <label for="dno">Genero:</label>
            <select class="form-control" id="gender" name="gender" value="<?php echo !empty($genderId) ? $genderId : ''; ?>" required>
                <?php foreach ($gendersList as $author) : ?>
                    <tr>
                        <option value="<?php echo htmlspecialchars($author['id']); ?>" <?php echo $author['id'] == $genderId ? "selected" : '' ?>><?php echo htmlspecialchars($author['name']); ?></option>
                    </tr>
                <?php endforeach; ?>
            </select>
        </div>

        <div class="form-group">
            <label for="dno">Referente ao autor:</label>
            <select class="form-control" id="author" name="author" value="<?php echo !empty($authorId) ? $authorId : ''; ?>" required>
                <?php foreach ($authorsList as $author) : ?>
                    <tr>
                        <option value="<?php echo htmlspecialchars($author['id']); ?>" <?php echo $author['id'] == $genderId ? "selected" : '' ?>><?php echo htmlspecialchars($author['name']); ?></option>
                    </tr>
                <?php endforeach; ?>
            </select>
        </div>


        <input class="btn btn-primary" type="submit" value="Cadastrar">
    </form>
</div>
<?php
include('../../templates/footer.php');
?>