<?php

use ConexaoPHPPostgres\AuthorModel;
use ConexaoPHPPostgres\AuthorAndBooksModel;
use ConexaoPHPPostgres\BookModel;

include '../database/models.php';
include_once '../database/database.ini.php';


try {

    $authorModel = new AuthorModel($pdo);
    $authorAndBooks = new AuthorAndBooksModel($pdo);
    $bookModel = new BookModel($pdo);


    $authorsList = $authorModel->all();
} catch (\PDOException $e) {
    echo $e->getMessage();
}

?>

<?php
include('../templates/header.php');
?>
<div class="container">

    <div class="row">
        <div class="col-auto mr-auto">
            <h1 style="padding-top: 10px; padding-bottom:10px">Autores</h1>
        </div>
        <div class="col-auto">
            <div class="text-right mb-4">
                <a class="btn" style="background-color:#00897c; color:white" href="../../pages/create/author.php">Cadastrar
                    novo</a>
            </div>
        </div>
    </div>

    <?php if (!empty($authorsList)) : ?>

    <!--Lista de autores-->
    <?php foreach ($authorsList as $author) : ?>

        <div>
            <div class="alert container">
                <div class="card-body" style="background-color: #F4F6FC;">
                    <div class="row" style="padding-bottom: 5px;">
                        <div class="col-sm-1">
                            <img src="../assets/icons/author-icon.png" height="70">
                        </div>
                        <div class="col-sm-8">
                            <h5 class="card-title">Nome: <?php echo htmlspecialchars($author['name']); ?></h5>
                            <h6 class="card-subtitle mb-2 text-muted"><?php echo htmlspecialchars($author['birth']); ?></h6>
                        </div>
                    </div>

                    <?php
                    $authorAndBooksList = $authorAndBooks->select_by_author($author['id']);
                    ?>



                    <div>


                        <?php if (!empty($authorAndBooksList)) : ?>

                        <!-- Lista de livros -->
                        <div class="row">
                            <div class="col" style="text-align: center;font-size: xx-large">
                                <p class="card-text mb-2"><img style="width: 50px" src="../assets/icons/books_icon.png">
                                    Livros:
                                </p>
                            </div>
                            <div class="col col-lg-1">

                            </div>
                        </div>

                        <div class="alert alert-light" role="alert" style="padding: 20px">

                            <table id="t01">
                                <?php foreach ($authorAndBooksList as $bookEach) : ?>


                                    <tr>
                                        <td><?php
                                            $book = $bookModel->get_by_id($bookEach['book_id']);
                                            echo htmlspecialchars($book['name']);
                                            ?></td>
                                    </tr>

                                <?php endforeach; ?>
                                <?php endif; ?>
                            </table>


                        </div>
                    </div>

                    <hr>
                    <a href="../../pages/update/author.php?id=<?php echo $author['id']; ?>" class="card-link">Editar</a>
                    <a href="../../pages/delete/author.php?id=<?php echo $author['id']; ?>" class="card-link">Remover</a>

                </div>
            </div>

        </div>
    <?php endforeach; ?>

     <?php else: ?>

        <div class="row">
            <div class="col" style="text-align: center;font-size: xx-large;padding-top: 40px">
                <h1>Lista Vazia</h1>
            </div>
            <div class="col col-lg-1">

            </div>
        </div>



    <?php endif; ?>
</div>


<?php
include('../templates/footer.php');
?>