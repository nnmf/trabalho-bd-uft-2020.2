CREATE TABLE IF NOT EXISTS author
(
    id    SERIAL       NOT NULL,
    name  VARCHAR(255) NOT NULL,
    birth DATE,
    Sex   CHAR(1),
    UNIQUE (name),
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS gender
(
    name VARCHAR(255) NOT NULL,
    id   SERIAL       NOT NULL,
    PRIMARY KEY (id),
    UNIQUE (name)
);


CREATE TABLE IF NOT EXISTS book
(
    name      VARCHAR(255) NOT NULL,
    id        SERIAL       NOT NULL,
    gender_id INT          NOT NULL,
    PRIMARY KEY (id),
    UNIQUE (name),
    FOREIGN KEY (gender_id) REFERENCES gender (id)
);


CREATE TABLE IF NOT EXISTS author_books
(
    id        SERIAL NOT NULL,
    author_id INT    NOT NULL,
    book_id   INT    NOT NULL,
    PRIMARY KEY (author_id, book_id, id),
    FOREIGN KEY (author_id) REFERENCES author (id),
    FOREIGN KEY (book_id) REFERENCES book (id)
);

INSERT INTO gender (name)
VALUES ('Ação e aventura'),
       ('Drama'),
       ('Comédia romântica'),
       ('Ficção científica'),
       ('Terror'),
       ('Biografia'),
       ('Outros');


