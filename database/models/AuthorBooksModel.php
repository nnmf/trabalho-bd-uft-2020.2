<?php

namespace ConexaoPHPPostgres;

class AuthorAndBooksModel
{
    private $pdo;

    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    public function all()
    {
        $stmt = $this->pdo->query('SELECT id, author_id, book_id FROM public.author_books');
        $stocks = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $stocks[] = [
                'id' => $row['id'],
                'author_id' => $row['author_id'],
                'book_id' => $row['book_id'],
            ];
        }
        return $stocks;
    }

    public function delete_by_author_id($id)
    {
        // Preparar pra remover
        $sql = "DELETE from author_books WHERE author_id='$id'";
        $stmt = $this->pdo->prepare($sql);
        // Executar o SQL
        $stmt->execute();
    }

    public function delete_by_book_id($id)
    {
        // Preparar pra remover
        $sql = "DELETE from author_books WHERE book_id='$id'";
        $stmt = $this->pdo->prepare($sql);
        // Executar o SQL
        $stmt->execute();
    }

    public function insert($authorId, $bookId)
    {
        // Preparar pra inserir novo exemplo
        $sql = "INSERT INTO public.author_books (author_id, book_id) VALUES (:authorId, :bookId)";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(':authorId', $authorId);
        $stmt->bindValue(':bookId', $bookId);
        // Executar
        $stmt->execute();
    }

    public function select_by_author($authorId)
    {
        $stmt = $this->pdo->query("SELECT book_id FROM public.author_books WHERE author_id='$authorId'");
        $stocks = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $stocks[] = [
                'book_id' => $row['book_id']
            ];
        }
        return $stocks;
    }

    public function select_authorId_by_bookId($bookId)
    {
        $stmt = $this->pdo->query("SELECT author_id FROM public.author_books WHERE book_id='$bookId'");
        $stocks = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $stocks[] = [
                'author_id' => $row['author_id']
            ];
        }
        return $stocks;
    }

    public function select_id_by_book($bookId)
    {
        $stmt = $this->pdo->query("SELECT id FROM public.author_books WHERE book_id='$bookId'");
        $stocks = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $stocks[] = [
                'id' => $row['id']
            ];
        }
        return $stocks;
    }

    public function select_by_essn_pdo($essn, $pdo)
    {

        $stmt = $this->pdo->query("SELECT essn, pno, hours FROM public.works_on WHERE essn='$essn' and pno='$pdo'");
        $workson = $stmt->fetch(\PDO::FETCH_ASSOC);
        if ($workson) {
            return [
                'essn' => $workson['essn'],
                'pno' => $workson['pno'],
                'hours' => $workson['hours'],
            ];
        } else {
            return null;
        }
    }


    public function update($authorId, $bookId)
    {   
        echo(" $authorId , $bookId ");
        $stmt = $this->pdo->prepare("UPDATE public.author_books SET author_id=:authorId WHERE book_id='$bookId'");
        $stmt->bindParam(':authorId', $authorId);
        $stmt->execute();
    }
}
