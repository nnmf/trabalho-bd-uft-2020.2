<?php

namespace ConexaoPHPPostgres;

class BookModel
{
    private $pdo;

    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    public function all()
    {
        $stmt = $this->pdo->query('SELECT name, id, gender_id FROM public.book');
        $stocks = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $stocks[] = [
                'name' => $row['name'],
                'id' => $row['id'],
                'gender_id' => $row['gender_id'],
            ];
        }
        return $stocks;
    }

    public function delete_by_id($id)
    {
        // Preparar pra remover
        $sql = "DELETE from book WHERE id='$id'";
        $stmt = $this->pdo->prepare($sql);
        // Executar o SQL
        $stmt->execute();
    }

    public function get_by_id($id)
    {
        $stmt = $this->pdo->query("SELECT name, gender_id FROM public.book WHERE id='$id'");
        $book = $stmt->fetch(\PDO::FETCH_ASSOC);
        return [
            'name' => $book['name'],
            'gender_id' => $book['gender_id']
        ];
    }

    public function get_id_by_name($name)
    {
        $stmt = $this->pdo->query("SELECT id FROM public.book WHERE name='$name'");
        $book = $stmt->fetch(\PDO::FETCH_ASSOC);
        return [
            'id' => $book['id'],
        ];
    }

    public function insert($name, $genderId)
    {
        // Preparar pra inserir novo exemplo
        $sql = "INSERT INTO public.book (name, gender_id) VALUES (:name, :genderId)";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(':name', $name);
        $stmt->bindValue(':genderId', $genderId);
        // Executar
        $stmt->execute();
    }

    public function update($name, $genderId, $id)
    {
        $sql = "UPDATE public.book SET name='$name', gender_id='$genderId' WHERE id='$id' ";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
    }
}
