<?php

namespace ConexaoPHPPostgres;

class GenderModel
{
    private $pdo;

    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    public function all()
    {
        $stmt = $this->pdo->query('SELECT name, id FROM public.gender');
        $stocks = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $stocks[] = [
                'name' => $row['name'],
                'id' => $row['id'],
            ];
        }
        return $stocks;
    }

    public function select_by_id($dnoid)
    {
        $stmt = $this->pdo->query("SELECT name,id  FROM public.gender WHERE id='$dnoid'");
        $gender = $stmt->fetch(\PDO::FETCH_ASSOC);
        return [
            'name' => $gender['name'],
            'id' => $gender['id'],

        ];
    }

    public function delete($dnumber)
    {
        $sql = "DELETE from department WHERE dnumber='$dnumber'";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
    }

    public function insert($dname, $mgr_ssn, $mgr_start_date)
    {
        // Preparar pra inserir novo exemplo
        $sql = "INSERT INTO public.department (dnumber, dname, mgr_ssn, mgr_start_date) VALUES (DEFAULT, :dname, :mgr_ssn, :mgr_start_date)";
        $stmt = $this->pdo->prepare($sql);
        $stmt->bindValue(':dname', $dname);
        $stmt->bindValue(':mgr_ssn', $mgr_ssn);
        $stmt->bindValue(':mgr_start_date', $mgr_start_date);
        // Executar
        $stmt->execute();
    }

    public function update($dnumber, $dname, $mgr_ssn)
    {
        $sql = "UPDATE public.department SET dname='$dname', mgr_ssn='$mgr_ssn' WHERE dnumber='$dnumber' ";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();
    }

}
