<?php

namespace ConexaoPHPPostgres;

class AuthorModel
{
    private $pdo;

    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    public function all()
    {
        $stmt = $this->pdo->query('SELECT id, name, birth, sex,id FROM public.author '
            . 'ORDER BY name ASC ');
        $stocks = [];
        while ($row = $stmt->fetch(\PDO::FETCH_ASSOC)) {
            $stocks[] = [
                'id' => $row['id'],
                'name' => $row['name'],
                'birth' => $row['birth'],
                'sex' => $row['sex']
            ];
        }
        return $stocks;
    }

    public function insert($name, $birth, $sex)
    {
        // Preparar pra inserir novo exemplo
        $sql = "INSERT INTO author (name,birth, sex) VALUES (:name, :birth,:sex)";
        $stmt = $this->pdo->prepare($sql);

        // Passar os valores
        $stmt->bindValue(':name', $name);
        $stmt->bindValue(':birth', $birth);
        $stmt->bindValue(':sex', $sex);

        // Executar
        $stmt->execute();
    }

    public function update($name, $birth, $sex, $id)
    {
        // Preparar pra atualizar novo exemplo
        $sql = "UPDATE public.author SET name='$name', birth='$birth', sex='$sex' WHERE id='$id' ";
        $stmt = $this->pdo->prepare($sql);
        // Executar
        $stmt->execute();
    }

    public function delete_by_id($id)
    {
        // Preparar pra remover 
        $sql = "DELETE from author WHERE id='$id'";
        $stmt = $this->pdo->prepare($sql);
        // Executar o SQL
        $stmt->execute();
    }

    public function select_by_id($id)
    {
        $stmt = $this->pdo->query("SELECT name, birth,sex FROM public.author WHERE id='$id' ");
        $author = $stmt->fetch(\PDO::FETCH_ASSOC);
        if ($author) {
            return [
                'name' => $author['name'],
                'birth' => $author['birth'],
                'sex' => $author['sex']
            ];
        } else {
            return null;
        }
    }
}
